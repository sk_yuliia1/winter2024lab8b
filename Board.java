import java.util.Random;
public class Board{
	private Tile[][] grid;
	private final int size;
	
	public Board(){
		this.size = 5;
		this.grid = new Tile[this.size][this.size];
		Random rand = new Random();
		
		//This loop puts some tile values on the board
		for(int i = 0; i<grid.length;i++){
			int randIndex = rand.nextInt(this.size);
			for(int j = 0; j < grid[i].length;j++){
				//Changes BLANK tile to a HIDDEN_WALL at a random generated index
				if(j == randIndex){
					this.grid[i][j] = Tile.HIDDEN_WALL;
				} else {
					this.grid[i][j] = Tile.BLANK;;
				}
			}
		}
	}
	//Returns a string that represents the board
	public String toString(){
		String result = "";
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				result += this.grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	
	//Returns int values based on the input entered by user and also based on the values of TILES placed on the game board
	public int placeToken(int column, int row){
		if(row >= this.size && column >= this.size || row < 0 && column < 0){
			return -2;
		}
		if(this.grid[column][row] == Tile.WALL || this.grid[column][row] == Tile.CASTLE){
			return -1;
		} else if (this.grid[column][row] == Tile.HIDDEN_WALL){
			this.grid[column][row] = Tile.WALL;
			return 1;
		} else { 
			this.grid[column][row] = Tile.CASTLE;
			return 0;
		}
	}
	
	
}