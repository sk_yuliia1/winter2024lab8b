import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		System.out.println("~~Welcome to my Board Game!~~");
		System.out.println("**You have only 7 turns!**");
		
		Scanner reader = new Scanner(System.in);
		Board board = new Board();
		
		int numCastles = 7;
		int turns = 0;
		
		//Works until the number of castles 
		while(numCastles > 0 && turns < 8){
			System.out.println("Number of Castles: " + numCastles);
			System.out.println("Number of Turns: " + turns);
			System.out.println(board);
			
			//Asks user to enter some values that will represent a column and a row
			System.out.println("Enter an integer to represent a row (0-4):");
			int row = reader.nextInt();
			System.out.println("Enter an integer to represent a column(0-4):");
			int column = reader.nextInt();
			
			//Stores result of placeToken method in a variable
			int result = board.placeToken(row, column);
			
			//Asks user to re-enter new values if the value of result is negative(invalid value)
			while(result < 0){
				System.out.println("Please, re-enter the values!");
				System.out.println("Enter an integer to represent a row(0-4):");
				row = reader.nextInt();
				System.out.println("Enter an integer to represent a column(0-4):");
				column = reader.nextInt();
				result = board.placeToken(column, row);
			} 
			//Tells user if the wall was at the entered position or his castle was successfully placed
			if(result == 1){
				System.out.println("There is a wall at that position!" + "\n" +  "row " + row + " and column " + column);	
			}
			if(result == 0){
				System.out.println("Castle tile was successfully placed!");
				numCastles--;
			}
			
			turns++;
		}
		//Prits the board before the final result of the game
		System.out.println(board);
		
		//Checks if the player wins 
		if(numCastles == 0){
			System.out.println("You win! Good job.");
		} else {
			System.out.println("Better luck next time..");
		}
		
	}
	
}